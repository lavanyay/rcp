table end_time {
 reads {
       per_packet.arrival_timestamp;
       per_packet.packet_length;
 }
 actions {
       set per_packet.end_time;
 }
}
 
// do_on_packet_arrival
control ingress {
	apply_table end_time;
	if (per_packet.end_time <= per_interval.end_slot) {
	   update_act_input_traffic(per_packet.packet_length);
	      } else {
	      // no apply tables, just calculating stuff, two values at a time!?
	         calculate_part2-1();
	         calculate_part2-2();
		 ..		 
	         calculate_part2-5();
		 calculate_part1();
		 update_act_input_traffic(register_local_var.part1);
   		 update_traffic_spill(register_local_var.part2);
		 }	
}


// do_on_packet_dequeue
control egress {	
	if (rcp.packet_type == RCP_SYN || .. RCP_REF || .. RCP_DATA) {
	   // fill_in_feedback();	   
	   if (rcp.request_rate < 0 ||rcp.request_rate > per_interval.flow_rate) {
	   // comparing per packet header to per interval metadata (register?)
	      modify_field(rcp.flow_rate, per_interval.flow_rate_);
	      }
	   update_running_avg_rtt1(RTT_GAIN);
	   update_running_avg_rtt2(RTT_GAIN);
	   update_running_avg_rtt3(RTT_GAIN);

	   modify_field(per_interval.dequeue_queue, per_packet.dequeue_queue);
	   }
}


// WITH A LOOK UP TABLE on raw per_interval values
table flow_rate1 {
      reads {
      	    per_interval.act_input_traffic_;
	    per_interval.dequeue_queue;
	    per_interval.avg_rtt;
	    per_interval.flow_rate;
	    }
      actions {
            set_flow_rate;
	    }
      max_size : !!;
}

!!control per-interval {
    //     ratio = (1 + ((Tq_/avg_rtt_)*(alpha_*(link_capacity_ - input_traffic_)
    //  - beta_*(Q_/avg_rtt_)))/link_capacity_);
    //    temp = flow_rate_ * ratio;

   // per_interval.act_input_traffic_, per_interval.dequeue_queue, per_interval.avg_rtt
   // per_interval.flow_rate_

   //   input_traffic_ = act_input_traffic_/Tq_; // bytes per second
   get_input_traffic()

   // (alpha_*(link_capacity_ - input_traffic_)
   get_extra_service()

   // (alpha_*(link_capacity_ - input_traffic_)
   get_queue_drain_rate()

   // (alpha_*(link_capacity_ - input_traffic_)  - beta_*(Q_/avg_rtt_))
   get_input_err()

   // (Tq_/avg_rtt_)
   get_scale()

   // (Tq_/avg_rtt_) * (alpha_*(link_capacity_ - input_traffic_)  - beta_*(Q_/avg_rtt_))
   scale_input_err()

   // num_flows =  link_capacity_ / flow_rate_
   get_num_flows()

   // (Tq_/avg_rtt_) * (alpha_*(link_capacity_ - input_traffic_)  - beta_*(Q_/avg_rtt_)) / ..
   divide_field(reg.input_err, reg.num_flows)   

   add_to_field_field(per_interval.flow_rate, reg.input_err)

}