////////////////////////
// headers.p4
////////////////////////

standard metadata {
 fields {
 	packet_length : 16
	}
}

header_type per_packet {
 fields {	      
	/* .. */
	arrival_timestamp : 48 // available at ingress
	enqueue_timestamp: 48 // available in egress
	dequeue_queue : 16 // available on dequeue
	end_time : 48 // to calculate
	}
}

header_type per_interval {
 fields {
 	act_input_traffic : 32
	traffic_spill_ : 32
	dequeue_queue : 32
	end_slot : 32
	avg_rtt : 32
	flow_rate : 32
	}
}

header_type rcp {
 fields {
 	packet_type : 8
	request_rate : 32
	flow_rate : 32
	rtt : 32
	}
}

header_type register_local_var {
 fields {
 	part1 = 32;
	part2 = 32;
	}
}

header_type register_temp {
fields {
 	temp1 = 32;
	temp2 = 32;
	temp3 = 32;
	}	
}

register per_interval {
	 layout per_interval;
	 static;
	 instance_count: 1;
}

// CAN ALSO JUST BE IN PACKET HEADER
// OR IN SWITCH METADATA
register temp {
	 layout register_temp;
	 static;
	 instance_count: 1
	 }

register local_var {
	 layout register_local_var;
	 static;
	 instance_count: 1
	 }
