
/////////////////////////
// actions.p4
////////////////////////

action set_end_time(value) {
       modify_field(register_local_var.end_time, value);
}

action update_act_input_traffic(size) {
       add_to_field(per_interval.act_input_traffic, size);
}

action update_traffic_spill(size) {
       add_to_field(per_interval.traffic_spill, size);
}

action update_running_avg_rtt(rtt_gain) {
// split into separate actions, so primtive actions in same "action"
// have parallel semantics
// 	avg = gain * var_sample + ( 1 - gain) * var_last_avg;
   // action 1
       modify_field(reg_temp1, rcp.rtt);
   // action 2
       !!multiply_field(reg_temp1, rtt_gain);   
   // action 1
       !!multiply_field(per_interval.avg_rtt, (1-rtt_gain));
   // action 3
       add_to_field(per_interval.avg_rtt, reg_temp1);
}

calculate_part2() {
// split into separate actions, so primtive actions in same "action"
// have parallel semantics
//     part2 = size * (end_time - end_slot_)/pkt_time_;
       // action 1
       modify_field(reg_temp2, per_packet.end_time);
       // action 2
       modify_field(reg_pkt_time_, per_packet.end_time)
       // action 3
       add_to_field(reg_pkt_time, per_packet.ingress_time);

       // adding one register value to another
       // action 3
       add_to_field(reg_temp2, -per_interval.end_slot);
       // action 4
       !!multiply_field(reg_temp2, size);
       // action 5
       !!!divide_field(reg_temp2, reg_pkt_time_);
}

calculate_part1() {
  modify_field(register_local_var.part1, per_packet.packet_length);
  add_to_field(register_local_var.part1, -register_local_var.part2));
}

// ACTIONS TO SET FLOW RATE
// ACTUALLY EACH ACTION SHOULD HAVE PARALLEL SEMANTICS

action get_input_traffic {
   modify.field(reg.input_traffic, per_interval.act_input_traffic)
   divide_field(reg.input_traffic, TQ_)
}
action get_extra_service {
   modify_field(reg.extra_service, link_capacity_)
   add_to_field(reg.extra_service, -reg.input_traffic)
   multiply_field_constant(reg.extra_service, ALPHA)
}
action get_queue_drain_rate() {
   modify_field(reg.queue_drain, per_interval.dequeue_queue)
   // !! divide one register by another
   divide_field(reg.queue_drain, per_interval.avg_rtt_)
   multiply_field_constant(reg.queue_drain, BETA)
}
action get_input_err() {
   modify_field(reg.input_err, req.extra_service)
   add_to_field(reg.input_err, -reg.queue_drain)
}
action get_scale() {
   modify_field(reg.scale, TQ_)
   divide_field(reg.scale, per_interval.avg_rtt)
}
action scale_input_err() {
   multiply_field(reg.input_err, reg.scale)
}
action get_num_flows() {
   modify_field(reg.num_flows, CAPACITY_LINK_)
   divide_field(reg.num_flows, per_interval.flow_rate_)
}
